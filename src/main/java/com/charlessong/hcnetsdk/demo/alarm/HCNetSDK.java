package com.charlessong.hcnetsdk.demo.alarm;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.win32.StdCallLibrary;

import java.util.Arrays;
import java.util.List;

/**
 * @author charles
 * @Date 2021/11/30
 */
public interface HCNetSDK extends StdCallLibrary {
    HCNetSDK INSTANCE = (HCNetSDK) Native.loadLibrary("C:\\Users\\胥耀\\Desktop\\2-报警布防监听\\lib\\HCNetSDK.dll",
            HCNetSDK.class);

    //初始化
    boolean NET_DVR_Init();

    //释放资源
    boolean NET_DVR_Cleanup();

    int NET_DVR_Login_V40(NET_DVR_USER_LOGIN_INFO pLoginInfo, NET_DVR_DEVICEINFO_V40 lpDeviceInfo);

    int NET_DVR_GetLastError();

    //注销
    boolean NET_DVR_Logout(int lUserID);

    //布防
    int NET_DVR_SetupAlarmChan_V41(int lUserID, NET_DVR_SETUPALARM_PARAM lpSetupParam);
    //撤防
    boolean NET_DVR_CloseAlarmChan_V30(int lAlarmHandle);

    //建立长链接、开始查询人员
    int NET_DVR_StartRemoteConfig(int lUserID, int dwCommand, Pointer lpInBuffer, int dwInBufferLen, FRemoteConfigCallback cbStateCallback, Pointer pUserData);

    //获取下一个人员信息
    int NET_DVR_GetNextRemoteConfig(int lHandle, Pointer lpOutBuff, int dwOutBuffSize);

    int NET_DVR_SendWithRecvRemoteConfig(int lHandle, Pointer lpInBuff, int dwInBuffSize, Pointer lpOutBuff, int dwOutBuffSize, IntByReference dwOutDataLen);

    //关闭长链接、释放资源
    boolean NET_DVR_StopRemoteConfig(int lHandle);

    //布防：设置回调
    boolean NET_DVR_SetDVRMessageCallBack_V31(FMSGCallBack_V31 fMessageCallBack, Pointer pUser);

    public static final int COMM_ALARM_ACS = 0x5002; //门禁主机报警信息

    public static final int MAX_NAMELEN = 16;    //DVR本地登陆名

    //门禁主机报警信息结构体
    public static class NET_DVR_ACS_ALARM_INFO extends Structure {
        public int dwSize;
        public int dwMajor; //报警主类型，参考宏定义
        public int dwMinor; //报警次类型，参考宏定义
        public NET_DVR_TIME struTime = new NET_DVR_TIME(); //时间
        public byte[] sNetUser = new byte[MAX_NAMELEN];//网络操作的用户名
        public NET_DVR_IPADDR struRemoteHostAddr = new NET_DVR_IPADDR();//远程主机地址
        public NET_DVR_ACS_EVENT_INFO struAcsEventInfo = new NET_DVR_ACS_EVENT_INFO(); //详细参数
        public int dwPicDataLen;   //图片数据大小，不为0是表示后面带数据
        public Pointer pPicData;
        public short wInductiveEventType; //归纳事件类型，0-无效，客户端判断该值为非0值后，报警类型通过归纳事件类型区分，否则通过原有报警主次类型（dwMajor、dwMinor）区分
        public byte byPicTransType;        //图片数据传输方式: 0-二进制；1-url
        public byte byRes1;             //保留字节
        public int dwIOTChannelNo;    //IOT通道号
        public Pointer pAcsEventInfoExtend;    //byAcsEventInfoExtend为1时，表示指向一个NET_DVR_ACS_EVENT_INFO_EXTEND结构体
        public byte byAcsEventInfoExtend;    //pAcsEventInfoExtend是否有效：0-无效，1-有效
        public byte byTimeType; //时间类型：0-设备本地时间，1-UTC时间（struTime的时间）
        public byte[] byRes = new byte[10];
    }

    public static class NET_DVR_IPADDR extends Structure {
        public byte[] sIpV4 = new byte[16];
        public byte[] byRes = new byte[128];

        public String toString() {
            return "NET_DVR_IPADDR.sIpV4: " + new String(sIpV4) + "\n" + "NET_DVR_IPADDR.byRes: " + new String(byRes) + "\n";
        }
    }

    //门禁主机事件信息
    public static class NET_DVR_ACS_EVENT_INFO extends Structure {
        public int dwSize;
        public byte[] byCardNo = new byte[32];
        public byte byCardType;
        public byte byAllowListNo;
        public byte byReportChannel;
        public byte byCardReaderKind;
        public int dwCardReaderNo;
        public int dwDoorNo;
        public int dwVerifyNo;
        public int dwAlarmInNo;
        public int dwAlarmOutNo;
        public int dwCaseSensorNo;
        public int dwRs485No;
        public int dwMultiCardGroupNo;
        public short wAccessChannel;
        public byte byDeviceNo;
        public byte byDistractControlNo;
        public int dwEmployeeNo;
        public short wLocalControllerID;
        public byte byInternetAccess;
        public byte byType;
        public byte[] byRes = new byte[20];
    }


    public static final int NET_SDK_CONFIG_STATUS_SUCCESS = 1000;
    public static final int NET_SDK_CONFIG_STATUS_NEEDWAIT = 1001;
    public static final int NET_SDK_CONFIG_STATUS_FINISH = 1002;
    public static final int NET_SDK_CONFIG_STATUS_FAILED = 1003;
    public static final int NET_SDK_CONFIG_STATUS_EXCEPTION = 1004;



    public static interface FRemoteConfigCallback extends StdCallLibrary.StdCallCallback {
        public void invoke(int dwType, Pointer lpBuffer, int dwBufLen, Pointer pUserData);
    }


    public static final int NET_DVR_GET_CARD = 2560;

    public int ACS_CARD_NO_LEN = 32;  //门禁卡号长度
    public int MAX_GROUP_NUM_128 = 128; //最大群组数
    public int MAX_DOOR_NUM_256 = 256; //最大门数
    public int CARD_PASSWORD_LEN = 8;   //卡密码长度
    public int MAX_CARD_READER_NUM = 64;  //最大读卡器数
    public int MAX_DOOR_CODE_LEN = 8; //房间代码长度
    public int MAX_LOCK_CODE_LEN = 8; //锁代码长度
    public int MAX_CARD_RIGHT_PLAN_NUM = 4; //卡权限最大计划个数

    public static class NET_DVR_CARD_RECORD extends Structure {
        public int dwSize;
        public byte[] byCardNo = new byte[ACS_CARD_NO_LEN];
        public byte byCardType;
        public byte byLeaderCard;
        public byte byUserType;
        public byte byRes1;
        public byte[] byDoorRight = new byte[MAX_DOOR_NUM_256];
        public NET_DVR_VALID_PERIOD_CFG struValid = new NET_DVR_VALID_PERIOD_CFG();
        public byte[] byBelongGroup = new byte[MAX_GROUP_NUM_128];
        public byte[] byCardPassword = new byte[CARD_PASSWORD_LEN];
        public short[] wCardRightPlan = new short[MAX_DOOR_NUM_256];
        public int dwMaxSwipeTimes;
        public int dwSwipeTimes;
        public int dwEmployeeNo;
        public byte[] byName = new byte[NAME_LEN];
        //按位表示，0-无权限，1-有权限
        //第0位表示：弱电报警
        //第1位表示：开门提示音
        //第2位表示：限制客卡
        //第3位表示：通道
        //第4位表示：反锁开门
        //第5位表示：巡更功能
        public int dwCardRight;
        public byte[] byRes = new byte[256];
    }

    //有效期参数结构体
    public static class NET_DVR_VALID_PERIOD_CFG extends Structure {
        public byte byEnable;
        public byte[] byRes1 = new byte[3];
        public NET_DVR_TIME_EX struBeginTime;
        public NET_DVR_TIME_EX struEndTime;
        public byte[] byRes2 = new byte[32];
    }

    public static class NET_DVR_TIME_EX extends Structure {
        public short wYear;
        public byte byMonth;
        public byte byDay;
        public byte byHour;
        public byte byMinute;
        public byte bySecond;
        public byte byRes;
    }

    public static final int SERIALNO_LEN = 48;   //序列号长度

    //NET_DVR_Login_V30()参数结构
    public static class NET_DVR_DEVICEINFO_V30 extends Structure {
        public byte[] sSerialNumber = new byte[SERIALNO_LEN];  //序列号
        public byte byAlarmInPortNum;    //报警输入个数
        public byte byAlarmOutPortNum;   //报警输出个数
        public byte byDiskNum;           //硬盘个数
        public byte byDVRType;         //设备类型, 1:DVR 2:ATM DVR 3:DVS ......
        public byte byChanNum;         //模拟通道个数
        public byte byStartChan;      //起始通道号,例如DVS-1,DVR - 1
        public byte byAudioChanNum;    //语音通道数
        public byte byIPChanNum;     //最大数字通道个数，低位
        public byte byZeroChanNum;    //零通道编码个数 //2010-01-16
        public byte byMainProto;      //主码流传输协议类型 0-private, 1-rtsp,2-同时支持private和rtsp
        public byte bySubProto;        //子码流传输协议类型0-private, 1-rtsp,2-同时支持private和rtsp
        public byte bySupport;        //能力，位与结果为0表示不支持，1表示支持，
        public byte bySupport1;        // 能力集扩充，位与结果为0表示不支持，1表示支持
        public byte bySupport2; /*能力*/
        public short wDevType;              //设备型号
        public byte bySupport3; //能力集扩展
        public byte byMultiStreamProto;//是否支持多码流,按位表示,0-不支持,1-支持,bit1-码流3,bit2-码流4,bit7-主码流，bit-8子码流
        public byte byStartDChan;        //起始数字通道号,0表示无效
        public byte byStartDTalkChan;    //起始数字对讲通道号，区别于模拟对讲通道号，0表示无效
        public byte byHighDChanNum;        //数字通道个数，高位
        public byte bySupport4;        //能力集扩展
        public byte byLanguageType;// 支持语种能力,按位表示,每一位0-不支持,1-支持
        //  byLanguageType 等于0 表示 老设备
        //  byLanguageType & 0x1表示支持中文
        //  byLanguageType & 0x2表示支持英文
        public byte byVoiceInChanNum;   //音频输入通道数
        public byte byStartVoiceInChanNo; //音频输入起始通道号 0表示无效
        public byte bySupport5;
        public byte bySupport6;   //能力
        public byte byMirrorChanNum;    //镜像通道个数，<录播主机中用于表示导播通道>
        public short wStartMirrorChanNo;  //起始镜像通道号
        public byte bySupport7;   //能力
        public byte byRes2;        //保留
    }

    public static interface FLoginResultCallBack extends StdCallLibrary.StdCallCallback {
        public int invoke(int lUserID, int dwResult, NET_DVR_DEVICEINFO_V30 lpDeviceinfo, Pointer pUser);
    }

    public static final int NET_DVR_DEV_ADDRESS_MAX_LEN = 129;
    public static final int NET_DVR_LOGIN_USERNAME_MAX_LEN = 64;
    public static final int NET_DVR_LOGIN_PASSWD_MAX_LEN = 64;

    //NET_DVR_Login_V40()参数
    public static class NET_DVR_USER_LOGIN_INFO extends Structure {
        public byte[] sDeviceAddress = new byte[NET_DVR_DEV_ADDRESS_MAX_LEN];
        public byte byUseTransport;
        public short wPort;
        public byte[] sUserName = new byte[NET_DVR_LOGIN_USERNAME_MAX_LEN];
        public byte[] sPassword = new byte[NET_DVR_LOGIN_PASSWD_MAX_LEN];
        public FLoginResultCallBack cbLoginResult;
        public Pointer pUser;
        public boolean bUseAsynLogin;
        public byte[] byRes2 = new byte[128];
    }


    //NET_DVR_Login_V40()参数
    public static class NET_DVR_DEVICEINFO_V40 extends Structure {
        public NET_DVR_DEVICEINFO_V30 struDeviceV30 = new NET_DVR_DEVICEINFO_V30();
        public byte bySupportLock;
        public byte byRetryLoginTime;
        public byte byPasswordLevel;
        public byte byRes1;
        public int dwSurplusLockTime;
        public byte[] byRes2 = new byte[256];

    }

    //布防参数
    public static class NET_DVR_SETUPALARM_PARAM extends Structure {
        public int dwSize;
        public byte byLevel; //布防优先级，0-一等级（高），1-二等级（中），2-三等级（低）
        public byte byAlarmInfoType; //上传报警信息类型（抓拍机支持），0-老报警信息（NET_DVR_PLATE_RESULT），1-新报警信息(NET_ITS_PLATE_RESULT)2012-9-28
        public byte byRetAlarmTypeV40; //0--返回NET_DVR_ALARMINFO_V30或NET_DVR_ALARMINFO, 1--设备支持NET_DVR_ALARMINFO_V40则返回NET_DVR_ALARMINFO_V40，不支持则返回NET_DVR_ALARMINFO_V30或NET_DVR_ALARMINFO
        public byte byRetDevInfoVersion; //CVR上传报警信息回调结构体版本号 0-COMM_ALARM_DEVICE， 1-COMM_ALARM_DEVICE_V40
        public byte byRetVQDAlarmType; //VQD报警上传类型，0-上传报报警NET_DVR_VQD_DIAGNOSE_INFO，1-上传报警NET_DVR_VQD_ALARM
        public byte byFaceAlarmDetection;
        public byte bySupport;
        public byte byBrokenNetHttp;
        public short wTaskNo;    //任务处理号 和 (上传数据NET_DVR_VEHICLE_RECOG_RESULT中的字段dwTaskNo对应 同时 下发任务结构 NET_DVR_VEHICLE_RECOG_COND中的字段dwTaskNo对应)
        public byte byDeployType;    //布防类型：0-客户端布防，1-实时布防
        public byte[] byRes1 = new byte[3];
        public byte byAlarmTypeURL;//bit0-表示人脸抓拍报警上传（INTER_FACESNAP_RESULT）；0-表示二进制传输，1-表示URL传输（设备支持的情况下，设备支持能力根据具体报警能力集判断,同时设备需要支持URL的相关服务，当前是”云存储“）
        public byte byCustomCtrl;//Bit0- 表示支持副驾驶人脸子图上传: 0-不上传,1-上传,(注：只在公司内部8600/8200等平台开放)
    }

    public static final int NAME_LEN = 32;    //用户名长度
    public static final int MACADDR_LEN = 6;      //mac地址长度
    //报警设备信息
    public static class NET_DVR_ALARMER extends Structure {
        public byte byUserIDValid;                 /* userid是否有效 0-无效，1-有效 */
        public byte bySerialValid;                 /* 序列号是否有效 0-无效，1-有效 */
        public byte byVersionValid;                /* 版本号是否有效 0-无效，1-有效 */
        public byte byDeviceNameValid;             /* 设备名字是否有效 0-无效，1-有效 */
        public byte byMacAddrValid;                /* MAC地址是否有效 0-无效，1-有效 */
        public byte byLinkPortValid;               /* login端口是否有效 0-无效，1-有效 */
        public byte byDeviceIPValid;               /* 设备IP是否有效 0-无效，1-有效 */
        public byte bySocketIPValid;               /* socket ip是否有效 0-无效，1-有效 */
        public int lUserID;                       /* NET_DVR_Login()返回值, 布防时有效 */
        public byte[] sSerialNumber = new byte[SERIALNO_LEN];    /* 序列号 */
        public int dwDeviceVersion;                /* 版本信息 高16位表示主版本，低16位表示次版本*/
        public byte[] sDeviceName = new byte[NAME_LEN];            /* 设备名字 */
        public byte[] byMacAddr = new byte[MACADDR_LEN];        /* MAC地址 */
        public short wLinkPort;                     /* link port */
        public byte[] sDeviceIP = new byte[128];                /* IP地址 */
        public byte[] sSocketIP = new byte[128];                /* 报警主动上传时的socket IP地址 */
        public byte byIpProtocol;                  /* Ip协议 0-IPV4, 1-IPV6 */
        public byte[] byRes2 = new byte[11];
    }

    public static class NET_DVR_CARD_COND extends Structure {
        public int dwSize;
        public int dwCardNum; //设置或获取卡数量，获取时置为0xffffffff表示获取所有卡信息
        public byte[] byRes = new byte[64];
    }

    public static class NET_DVR_CARD_SEND_DATA extends Structure {
        public int dwSize;
        public byte[] byCardNo = new byte[ACS_CARD_NO_LEN]; //卡号
        public byte[] byRes = new byte[16];
    }

    //校时结构参数
    public static class NET_DVR_TIME extends Structure {//校时结构参数
        public int dwYear;        //年
        public int dwMonth;        //月
        public int dwDay;        //日
        public int dwHour;        //时
        public int dwMinute;        //分
        public int dwSecond;        //秒

        public String toString() {
            return "NET_DVR_TIME.dwYear: " + dwYear + "\n" + "NET_DVR_TIME.dwMonth: \n" + dwMonth + "\n" + "NET_DVR_TIME.dwDay: \n" + dwDay + "\n" + "NET_DVR_TIME.dwHour: \n" + dwHour + "\n" + "NET_DVR_TIME.dwMinute: \n" + dwMinute + "\n" + "NET_DVR_TIME.dwSecond: \n" + dwSecond;
        }

        //用于列表中显示
        public String toStringTime() {
            return String.format("%02d/%02d/%02d%02d:%02d:%02d", dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond);
        }

        //存储文件名使用
        public String toStringTitle() {
            return String.format("Time%02d%02d%02d%02d%02d%02d", dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond);
        }
    }

    public static class BYTE_ARRAY extends Structure {
        public byte[] byValue;

        public BYTE_ARRAY(int iLen) {
            byValue = new byte[iLen];
        }

        @Override
        protected List<String> getFieldOrder() {
            // TODO Auto-generated method stub
            return Arrays.asList("byValue");
        }
    }



    public static interface FMSGCallBack_V31 extends StdCallLibrary.StdCallCallback {
        public boolean invoke(int lCommand, NET_DVR_ALARMER pAlarmer, Pointer pAlarmInfo, int dwBufLen, Pointer pUser);
    }
}
