package com.charlessong.hcnetsdk.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HcNetSdkDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HcNetSdkDemoApplication.class, args);
    }

}
